<?php
session_start();

$userid = null;

if (  !$_SESSION["authenticated"] || empty($_SESSION["userid"]) ) {
    header('Location: login.php');
} else {
    require_once('database.php');
    
    $userid = $_SESSION["userid"];

    $query = $connection->prepare("SELECT `user_firstname` FROM `users` WHERE `user_id` = ? ");
    $query->bind_param("i", $userid);
    $query->execute();
    $query->bind_result($username);
    $query->fetch();
    $query->close();

?>
<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Masterlist - El gestor de listas</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/modernizr.js"></script>
</head>
<body>

<div class="container">

    <nav class="top-bar hide-for-small" data-topbar="">
        <ul class="title-area">
            <li class="name">
                <h1>
                    <a href="/index.php">Masterlist</a>
                </h1>
            </li>
        </ul>
        <section class="top-bar-section">
            <ul class="right">
                <li class="divider"></li>
                <li class="has-dropdown not-click">
                    <a class="" href="#">Usuario: <?php echo $username; ?></a>
                    <ul class="dropdown">
                        <li><a href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li class="has-dropdown not-click">
                    <a class="" href="#">Inicio</a>
                    <ul class="dropdown">
                        <li><a href="features.php">Caracteristicas</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li class="has-dropdown not-click">
                    <a class="" href="#">Acciones</a>
                    <ul class="dropdown">
                        <li><a href="addlist.php">Nueva lista</a></li>
                        <li><a href="additem.php">Nuevo item</a></li>
                    </ul>
                </li>
            </ul>
        </section>
    </nav>


    <section class="search-container" role="main">
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>



        <div class="row hide-for-small"> &nbsp; </div>
        

        <form method="post" data-abide="ajax" id="myform">

            <div class="row">         
                <div class="small-12 small-centered medium-8 columns" >
                    <div data-alert class="alert-box radius" id="myAlert">
                        <span></span>
                        <a href="#" class="close">&times;</a>          
                    </div>
                </div>
            </div> 

            <div class="row">

                <div class="small-12 small-centered medium-8 columns">
                    
                    <div class="row collapse">
                        <div class="small-10 columns">
                            <input id="list-name" name="list-name" type="text" placeholder="Nombre de la lista">
                            <small class="error" id="list-name-error">Es necesario informar nombre de lista</small>
                        </div>
                        <div class="small-2 columns">
                            <input id="add-new-list" type="submit" value="Buscar" class="tiny button">
                        </div>
                    </div>
                
                </div>

            </div>
            <div class="row"> &nbsp; </div> 

        </form>

    </section>


    <section id="result-list" style="display:none;">
        <div class="row">
            <div class="small-12 small-centered medium-8 columns">
                <ul id="ul-list">
                    <li></li>
                </ul>
            </div>
        </div>
    </section>  

    <section id="footer">
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>        
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>
    </section>

</div>



<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/foundation/foundation.abide.js"></script>
<script src="js/foundation/foundation.alert.js"></script>

<script type="text/javascript">
jQuery(document).on('ready', function() {

    jQuery("#result-list").hide();
    jQuery("div#myAlert").hide();

    jQuery("#myform").on('valid.fndtn.abide', function() {
        jQuery("#result-list ul").empty();
        jQuery("div#myAlert span").html("");
        jQuery("div#myAlert").hide();

        var form = jQuery('form');
        var json = ConvertFormToJSON(form);

        jQuery.ajax({
            type: "POST",
            url: "search.php",
            data: json,
            dataType: "json"
        }).done(function(state){
            if (state.success === true) {
                jQuery("#result-list").show();
                jQuery.each(state.list, function(){
                    jQuery('#result-list ul').append('<li><a href="items.php?listid=' + 
                        this["listid"] + '"><strong>' + 
                        this["listname"] + '</strong> creada por '  + 
                        this["username"] + ' el ' + 
                        this["createdate"] + '</a></li>');    
                });
            } else {
                var msg = state.error.join();
                jQuery("div#myAlert").show();
                jQuery("div#myAlert span").html(msg);
                console.log(msg);
            }
        }).fail(function(state){
            alert('Fallo en la creacion de un nuevo item');
        }).always(function(){
            //alert('complete');
            //return true;
        });
    });

    /*jQuery("#myAlert").click();

    jQuery(document).on('close.fndtn.alert-box', function(event){
        console.info('An alert box has been closed!');
    });*/

});

function ConvertFormToJSON(form){
    var array = jQuery(form).serializeArray();
    var json = {};
    jQuery.each(array, function() {
        json[this.name] = this.value || '';
    });
    return json;
}

$(document).foundation();

//{ alert: {speed: 3000}}
</script>

</body>
</html>
<?php } ?>