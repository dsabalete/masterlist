<?php
session_start();

if (  !$_SESSION["authenticated"] || empty($_SESSION["userid"]) ) {
    header('Location: login.php');
} else {

    // Peticion POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (!empty($_SESSION["authenticated"]) && $_SESSION["authenticated"] == "true") {
            // ejecuta consulta y muestra resultados
            $listname = null;

            $result = array();
            $result['error'] = array();
            
            if (!empty($_POST["list-name"])) {
                $listname = $_POST["list-name"];
            } else {
                $listname = " ";
                //array_push($result['error'], 'Por favor especifica un nombre de lista');
            }


            if (isset($result['error']) && count($result['error']) > 0) {
                $result['success'] = false;
            } else {
                $result['success'] = true;

                // TODO Ejecutar la consulta y mostrar resultados
                require_once('database.php');

                $userid = $_SESSION["userid"];


                $query = $connection->prepare("SELECT l.list_id, u.user_firstname, l.list_name, l.create_date " .
                    " FROM `lists` AS l, `users` AS u " .
                    " WHERE u.user_id = l.user_id AND l.list_name LIKE ? ");
                $listname = '%'.$listname.'%';
                $query->bind_param("s", $listname);
                $query->execute();
                $query->bind_result($listid, $username, $listname, $createdate);
                $result['list'] = array();
                while ($query->fetch()) {
                    array_push($result['list'], 
                        array("listid" => $listid, 
                                "username" => $username, 
                                "listname" => $listname, 
                                "createdate" => $createdate));
                }

                $numrows = $query->num_rows;

                $query->free_result();
                $query->close();

                if ($numrows == 0) {
                    array_push($result['error'], 'No hay listas con ese nombre');
                    $result['success'] = false;
                }
            }

            /*
            if (!empty($_POST["new-task-date"]))
                $date = new DateTime($_POST["new-task-date"]);
            else 
                array_push($result['error'], 'Please specify a date for your task');

            if (!empty($_POST["new-task-desc"]))
                $desc = $_POST["new-task-desc"];

            if (!empty($_POST["new-task-email"]))
                $email = explode(',', $_POST["new-task-email"]);
            */
           
           echo json_encode($result);        

        } else {
            header('Location: login.php');
        }
    } else {
        // Peticion GET
    }    
}
?>