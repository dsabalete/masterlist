<?php
$username = null;
$password = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    require_once('database.php');

    if(!empty($_POST["username"]) && !empty($_POST["password"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
    
        $query = $connection->prepare("SELECT `user_id` FROM `users` WHERE `user_login` = ? and `user_password` = PASSWORD(?)");
        $query->bind_param("ss", $username, $password);
        $query->execute();
        $query->bind_result($userid);
        $query->fetch();
        $query->close();

        if(!empty($userid)) {
            session_start();
            $session_key = session_id();
            $_SESSION["authenticated"] = 'true';
            $_SESSION["userid"] = $userid;
            
            $query = $connection->prepare("INSERT INTO `sessions` ( `user_id`, `session_key`, `session_address`, `session_useragent`, `session_expires`) VALUES ( ?, ?, ?, ?, DATE_ADD(NOW(),INTERVAL 1 HOUR) );");
            $query->bind_param("isss", $userid, $session_key, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'] );
            $query->execute();
            $query->close();
            
            header('Location: index.php');
        }
        else {
            header('Location: login.php');
        }
        
    } else {
        header('Location: login.php');
    }
} else {
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Masterlist - Bienvenidos</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/modernizr.js"></script>
</head>
<body>

<div id="container">
    
    <section class="login-container">
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row">

            <div class="small-12 small-centered medium-6 large-4 columns">

                <form id="login" method="post">
                    <fieldset>
                        <legend>Masterlist: Login</legend>
                        <div class="row">
                            <div class="small-12 columns">
                                <label for="username">Usuario:
                                    <input id="username" name="username" type="text" required>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <label for="password">Password:
                                    <input id="password" name="password" type="password" required>                    
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <input type="submit" value="Login" class="right">
                            </div>
                        </div>
                    </fieldset>                    
                </form>
            </div>            
        </div>
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>
        <div class="row hide-for-small"> &nbsp; </div>
    </section>

</div>

</body>
</html>
<?php } ?>