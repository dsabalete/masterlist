use c9;

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) unsigned NOT NULL auto_increment,
  `user_login` varchar(100) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_firstname` varchar(50) NOT NULL,
  `user_surname` varchar(50) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_registered` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `idx_user_login_key` (`user_login`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` bigint(20) unsigned NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL REFERENCES `users`(`user_id`),
  `session_key` varchar(60) NOT NULL,
  `session_address` varchar(100) NOT NULL,
  `session_useragent` varchar(200) NOT NULL,
  `session_expires` datetime NOT NULL default '0000-00-00 00:00:00',  
  PRIMARY KEY (`session_id`),
  KEY `idx_session_key` (`session_key`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `lists` (
  `list_id` bigint(20) unsigned NOT NULL auto_increment,
  `user_id` bigint(20) NOT NULL REFERENCES `users`(`user_id`),
  `list_name` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY (`list_id`),
  KEY `idx_list_name_key` (`list_name`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` bigint(20) unsigned NOT NULL auto_increment,
  `list_id` bigint(20) NOT NULL REFERENCES `lists`(`list_id`),
  `tag_name` varchar(60) NOT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `idx_tag_name_key` (`tag_name`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` bigint(20) unsigned NOT NULL auto_increment,
  `list_id` bigint(20) NOT NULL REFERENCES `lists`(`list_id`),
  `item_name` varchar(60) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `idx_item_name_key` (`item_name`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `users` ( `user_login`, `user_password`, `user_firstname`, `user_surname`, `user_email`, `user_registered` 
) VALUES ( 'pepe', PASSWORD('secreto'), 'pepe', 'lopez', 'pepelopez@email.com', NOW() );

INSERT INTO `lists` ( `user_id`, `list_name`, `create_date` ) VALUES ( 1, 'Cosas de viaje', NOW() );

INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Pasaporte' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Maleta de tapa dura' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Pantalones invierno' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Camisetas' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Anorak' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Botas trekking' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Ropa interior' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Cepillo de dientes' );
INSERT INTO `items` ( `list_id`, `item_name` ) VALUES ( 1, 'Cargador movil' );