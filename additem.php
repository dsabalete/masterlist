<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Masterlist - Bienvenidos</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/modernizr.js"></script>
</head>
<body>

<!-- [container] -->
<div class="container">

    <!-- [top-bar] -->
    <nav class="top-bar hide-for-small" data-topbar="">
        <ul class="title-area">
            <li class="name">
                <h1>
                    <a href="/index.php">Masterlist</a>
                </h1>
            </li>
        </ul>
        <section class="top-bar-section">
            <ul class="right">
                <li class="divider"></li>
                <li class="has-dropdown not-click">
                    <a class="" href="/index.php">Inicio</a>
                    <ul class="dropdown">
                        <li><a href="/features.php">Caracteristicas</a></li>
                        <li><a href="/faq.php">FAQ</a></li>
                    </ul>
                </li>

            </ul>
        </section>
    </nav>
    <!-- [/top-bar] -->

    <!-- [scroll-container] -->
    <section class="scroll-container" role="main">

        <div class="row">

            <!-- [large-3 medium-4 columns] --> 
            <div class="large-3 medium-4 columns">
                <div class="hide-for-small">
                    <div class="sidebar">
                        <h5>Buscador de listas</h5>
                        <form>
                            <input id="autocomplete" type="search" placeholder="Busca otras listas" 
                                tabindex="1" autocomplete="off"></input>
                        </form>
                        <nav>
                            <ul class="side-nav">
                                <li class="heading">MenuMenu</li>
                                <li class="active"><a data-search="Index" href="index.php">Inicio</a></li>
                                <li class="active"><a data-search="Caracteristicas" href="features.php">Caracteristicas</a></li>
                                <li class="active"><a data-search="Ayuda" href="doc.php">Ayuda</a></li>

                            </ul>

                        </nav>
                    </div>

                </div>
            </div>
            <!-- [/large-3 medium-4 columns] --> 


            <!-- [large-9 medium-8 columns] -->
            <div class="large-9 medium-8 columns">
                <h1 id="planifica-tu-viaje">Planifica tu viaje</h1>
                <hr />

                <form data-abide="" novalidate="novalidate">
                    <fieldset>
                        <legend>Nuevo item de la lista</legend>

                        <div class="row">
                            <div class="large-4 medium-4 small-12 columns">
                                <label for="new-list-name">Nombre:</label>
                                <input title="Este campo es obligatorio." id="new-list-name" name="new-list-name" type="text" required>
                            </div>
                            <div class="large-4 medium-4 small-12 columns">
                                <label for="new-list-date">Fecha:</label>
                                <input title="Este campo es obligatorio." id="new-list-date" name="new-list-date" type="datetime" required>   
                            </div>
                            <div class="large-4 medium-4 small-12 columns">
                                <label for="new-list-priority">Prioridad:</label>
                                <input id="new-list-priority" name="new-list-priority" type="number" required min="1" max="5" step="1" value="2">
                            </div>            

                        </div>
                            
                        <div class="row">
                            <div class="large-4 medium-4 small-12 columns">
                                <label for="new-list-color">Color:</label>
                                <input id="new-list-color" name="new-list-color" type="color">
                            </div>
                            <div class="large-4 medium-4 small-12 columns">
                                <label for="new-list-desc">Descripcion:</label>
                                <input id="new-list-desc" name="new-list-desc" type="text">
                            </div>
                            <div class="large-4 medium-4 small-12 columns">
                                <label for="new-list-email">Invitar:</label>
                                <input id="new-list-email" name="new-list-email" type="email" multiple>
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-8 medium-8 columns"></div>
                            <div class="large-4 medium-4 small-12 columns">
                                <input id="add-new-list" type="submit" value="Añadir nueva tarea" class="small button">
                            </div>
                        </div>

                    </fieldset>
                </form>

            </div>
            <!-- [/large-9 medium-8 columns] -->

        </div>
    </section>
    <!-- [/scroll-container] -->


    <!-- [lista-resultados-large] -->
    <section id="lista-resultados-large">
        <div class="row">
            <div class="large-12 medium-12 columns">
                <h1 id="lista-tareas">Lista de tareas</h1>
                <hr />

                <table id="to-do-list">
                    <caption>¿Que es lo siguiente?</caption>
                    <thead>
                        <tr>
                            <th width="5%"> </th>
                            <th width="15%">Fecha</th>
                            <th width="5%">Prioridad</th>                            
                            <th width="15%">Nombre</th>
                            <th width="40%">Descripcion</th>
                            <th width="20%">Invitados</th>
                        </tr>
                    </thead>
                    <tbody>                        
                    </tbody>    
                </table>
            </div>
        </div>
    </section>
    <!-- [/lista-resultados-large] -->

</div>
<!-- [/container] -->



<script src="js/vendor/jquery.js"></script>
<script type="text/javascript">
$(document).on('ready', function() {

    // INI TEST TEST TEST
    $('#new-list-name').val('Vacuna');
    $('#new-list-date').val('22-10-2014');
    $('#new-list-priority').val('2');
    $('#new-list-color').val('#cc6633');
    $('#new-list-desc').val('Ir a vacunarse contra el paludismo');
    $('#new-list-email').val('coronel@tapioca.com');
    // END T EST TEST TEST

    $('#add-new-list').click(function(event){
        event.preventDefault();
        var tbody = $('#to-do-list > tbody');
        //alert(tbody;        
        tbody.append('<tr><td style="background-color:' + $('#new-list-color').val() + 
            '"><input type="checkbox" /></td><td>' + $('#new-list-date').val() +
            '</td><td>' + $('#new-list-priority').val() + '</td><td>' + $('#new-list-name').val() + 
            '</td><td>' + $('#new-list-desc').val() + '</td><td>' + $('#new-list-email').val() + '</td></tr>');
        
        //return false;
    });
});
</script>
  
</body>
</html>
