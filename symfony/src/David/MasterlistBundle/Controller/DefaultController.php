<?php

namespace David\MasterlistBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render(
            'DavidMasterlistBundle:Default:hola.html.twig', 
            array('name' => $name)
        );
    }
}
