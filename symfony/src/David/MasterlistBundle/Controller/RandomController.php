<?php

namespace David\MasterlistBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class RandomController extends Controller
{
    public function indexAction($limit)
    {
        $number = rand(1, $limit);
        
        return $this->render(
            'DavidMasterlistBundle:Random:random.html.twig',
            array('limit' => $limit, 'number' => $number)
        );
    }
}
