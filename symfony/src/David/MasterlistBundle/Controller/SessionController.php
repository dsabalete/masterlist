<?php

namespace David\MasterlistBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SessionController extends Controller 
{
    public function indexAction(Request $request)
    {
    	$session = $request->getSession();

    	// store an attribute for reuse during a later user request
    	$session->set('foo', 'bar');

    	// get the attribute set by another controller in another request
    	$foobar = $session->get('foobar');

    	// use a default value if the attribute doesn't exist
    	$filters = $session->get('filters', array());
    }
}