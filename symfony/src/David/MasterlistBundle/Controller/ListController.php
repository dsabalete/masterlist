<?php

namespace David\MasterlistBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller
{
    public function createAction(Request $request)
    {
    	$form = $this->createFormBuilder()
    		// ...
    		->getForm();

    	$form->handleRequest($request);
        
    	if ($form->isValid()) {
    		// perform some action...

    		return $this->redirect($this->generateUrl('task_success'));
    	}

        return $this->render('DavidMasterlistBundle:List:form.html.twig', 
        	array('accion' => 'Crear'));
    }

    public function searchAction()
    {
    	return $this->render('DavidMasterlistBundle:List:form.html.twig', 
    		array('accion' => 'Buscar'));
    }
}