<?php

namespace David\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use David\TaskBundle\Entity\Task;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }

    /**
     * @Route("/task/")
     */
    public function newAction(Request $request)
    {
    	// crear una tarea
    	$task = new Task();
    	// $task->setTask('Actualizar el pasaporte');
    	// $task->setDueDate(new \DateTime('tomorrow'));

    	$form = $this->createFormBuilder($task)
    		->add('task', 'text')
    		->add('dueDate', 'date')
    		->add('save', 'submit', array('label' => 'Create Task'))
    		->add('saveAndAdd', 'submit', array('label' => 'Save and Add'))
    		->getForm();

    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		// guardamos los datos en la base de datos y ...

    		// para saber que boton se pulsó
    		$nextAction = $form->get('saveAndAdd')->isClicked()
    			? 'task_new'
    			: 'task_success';

    		return $this->redirect($this->generateUrl($nextAction));
    	}

    	return $this->render('DavidTaskBundle:Default:new.html.twig', array(
    		'form' => $form->createView(),
    	));
    }
}
