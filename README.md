# Descripcion del proyecto
Checklister es un gestor de listas donde tanto se pueden crear listas como buscar dentro de ellas. Por ejemplo, si te quieres ir a la playa, te haces una lista de cosas para que no se te olvide nada. Puedes crear tu lista o ver listas que se ha creado otra gente. 

# Diseño
Una _lista_ es un conjunto de _items_, que ha creado un _usuario_ y a la cual le ha dado un _nombre_. La _lista_ puede tener asociados una serie de _tags_ para poder hacer busquedas filtradas. Por usuario, por item, por nombre (de la lista), por tags. 

# Segunda fase
Lo siguiente seria ir guardando los items que elige la gente para ver cuales son las cosas más típicas que la gente se lleva de viaje por ejemplo, se podrian añadir tags para filstrar segun que tipo de viaje, si es a la playa, a la montaña, de mochilero, etc. y otro paso más seria que te puedas crear un perfil, tener tus listas favoritas, compartirlas, etc...

# Instalación
Cargar el script sql/masterlist.sql en una base de datos MySQL antes de acceder a la aplicación para tener datos de prueba.
Cambiar los parametros de configuración del acceso a base de datos en el fichero 'database.php'.

# Comandos mysql
## start MySQL. Will create an empty database on first start
$ mysql-ctl start

## stop MySQL
$ mysql-ctl stop

## run the MySQL interactive shell
$ mysql-ctl cli

### Fichero de logs de apache: ~/lib/apache2/log
________________________________________________________________________________

### Url de pruebas
https://masterlist-c9-dsabalete.c9.io/login.php

### Recursos y ejemplos
http://www.developerdrive.com/2013/05/creating-a-simple-to-do-application-%E2%80%93-part-3/
http://ieatcss.com/zurb-foundation-framework.html
http://foundation.zurb.com/docs/css.html
http://foundation.zurb.com/forum/posts/530-beginner-foundation-5-tutorials
http://webdesign.tutsplus.com/tutorials/foundation-for-beginners-progress-alerts-tooltips-and-the-elusive-mega-drop--webdesign-13195